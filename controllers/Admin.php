<?php 

class CAdmin extends Controller{

	public function __construct() {
		parent::__construct();
	}

	// ENDPOINT
	public function test(){
		$return = "This is Controller";
		echo json_encode($return);
	}

	// NOT AN ENDPOINT
	private function test2(){
		$return = "Test API";
		echo json_encode($return);
	}

}



