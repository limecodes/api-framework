<?php

/*
** HOW TO CREATE AN ENDPOINT:
** + create a PUBLIC method in the child class
*/

require_once("config.php");
require_once("SQLHelper.php");

class Controller {

	protected $sql_obj = null;

	public function __construct(){
		$this->sql_obj = SQLHelper::get_instance();
	}

}