<?php
class Routes {

	public static function executeRoute($route) {
		$module = strtolower($route["module"]);
		$action = strtolower($route["action"]);

		Routes::findResource($module, $action);
	}

	public static function findResource($module, $action) {
		$module = ucwords($module);
		$modulePath = "endpoints/".$module.".php";
		if(!file_exists($modulePath))
			Endpoint::handle404();

		$classPath = "controllers/".$module.".php";
		if(file_exists($classPath)) {
			require_once("controllers/Controller.php");
			require_once($classPath);
		}

		require_once($modulePath);
		$class = new $module();
		$class->executeRoute($class, $action);
	}
}