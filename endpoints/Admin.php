<?php 

class Admin extends Endpoint{

	public function __construct() {
		parent::__construct();
	}

	// ENDPOINT
	public function test(){
		parent::validateMethod("get");
		$controller = parent::getController($this);
		$return = "Test API";
		$controller->test();
		echo json_encode($return);
	}

	// NOT AN ENDPOINT
	private function test2(){
		$return = "Test API";
		echo json_encode($return);
	}

}