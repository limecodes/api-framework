<?php

/*
** HOW TO CREATE AN ENDPOINT:
** + create a PUBLIC method in the child class
*/

class Endpoint {

	private $controller = null;

	public function __construct() {}

	public function executeRoute($obj, $action) {
		$isValid = is_callable(array($obj, $action));
		if ($isValid) {
			$obj->$action();
		} else {
			$this->handle404();
		}
	}

	protected function validateMethod($required) {
		$required = strtoupper($required);
		if($required == $_SERVER['REQUEST_METHOD'])
			return true;
		Endpoint::throwHTTPcode(405, "Invalid Method");
	}

	protected function getController($obj) {
		if($this->controller)
			return $this->controller;

		$class = "C".get_class($obj);

		if(class_exists($class)) {
			$this->controller = new $class();
		}

		return $this->controller;
	}

	protected function returnSuccess($data, $customHeaders = false) {
		http_response_code(200);
		if(!$customHeaders) {
			header("Content-type: application/json");
			echo json_encode($data);
		}
		exit();
	}

	protected function returnError($code, $data, $customHeaders = false) {
		http_response_code($code);
		if(!$customHeaders) {
			header("Content-type: application/json");
			echo json_encode($data);
		}
		exit();
	}

	public static function handle404() {
		Endpoint::throwHTTPcode(404, "Resource Not Found");
	}

	public static function handleParamError() {
		Endpoint::throwHTTPcode(400, "Parameter Error");
	}

	public static function throwHTTPcode($code, $msg) {
		http_response_code($code);
		$response = array();
		$response["msg"] = $msg;
		die(json_encode($response));
	}

}